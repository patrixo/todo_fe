import localforage from 'localforage'
import axios from './axios'
import store from './store/store'

import router from './router/router'

const auth = {
  saveToken (token) {
    return new Promise((resolve, reject) => {
      localforage.setItem('userAuthToken', token).then(() => {
        this.setHttpToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  saveRefreshToken (refreshToken) {
    return new Promise ((resolve, reject) => {
      localforage.setItem('refreshToken', refreshToken).then(() => {
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  saveUser (user) {
    return new Promise((resolve, reject) => {
      localforage.setItem('userAuthUser', user).then(() => {
        this.setUser(user)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  setHttpToken (token) {
    if (token) {
      axios.defaults.headers.common = { 'Authorization': 'Bearer ' + token }
    }
  },
  async getRefreshToken () {
    return await localforage.getItem('refreshToken')
  },
  isLoggedIn () {
    return new Promise((resolve, reject) => {
      localforage.getItem('userAuthToken').then(token => {
        if (token) {
          resolve()
        } else {
          reject(new Error('No token found'))
        }
      })
    })
  },
  loadToken () {
    return new Promise((resolve) => {
      localforage.getItem('userAuthToken').then(token => {
        if (token) {
          this.setHttpToken(token)
          this.loadUser().then(() => {
            resolve()
          })
        } else resolve()
      })
    })
  },
  loadUser () {
    return new Promise((resolve, reject) => {
      localforage.getItem('userAuthUser').then(user => {
        this.setUser(user)
        resolve(user)
      }).catch(error => {
        reject(error)
      })
    })
  },
  setUser (user) {
    store.state.security.user = user
    if (user === null) {
    store.state.security.isLoggedIn = false
    }
  },
  logout (expired = false) {
    localforage.removeItem('userAuthToken')
    localforage.removeItem('userAuthUser')
    localforage.removeItem('refreshToken')
    this.setUser(null)
    const route = { name: 'Login' }
    if (expired) route.query = { expired: true }
    router.push(route)
  }
}

export default auth
