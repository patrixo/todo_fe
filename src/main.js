import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router/router'
import auth from './auth'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Element from 'element-ui';
import VeeValidate from 'vee-validate';
import store from './store/store'
import Notifications from 'vue-notification'

//  plugins
import vClickOutside from 'v-click-outside'

//  css
import './assets/css/main.css'

Vue.use(VeeValidate);
Vue.use(Element, { size: 'small', zIndex: 3000 });
Vue.use(VueAxios, axios)
Vue.use(ElementUI);
Vue.use(VueRouter)
Vue.use(Notifications)

Vue.directive('clickOutside', vClickOutside.directive)

Vue.config.productionTip = false


Vue.config.productionTip = false

auth.loadToken().then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

  // new Vue({
  //   router,
  //   store,
  //   render: h => h(App)
  // }).$mount('#app')