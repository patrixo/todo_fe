import Vue from 'vue'
import Router from 'vue-router'
import beforeEach from './beforeEach'

import Layout from './../views/Layout'
import Registration from './../views/Registration'
import Login from './../views/Login'
import Notes from './../views/Notes/Notes.vue'

Vue.use(Router)

const routes = [
    {
        path: '',
        component: Layout,
        name: 'Home',
        children:  [
            {
                path: '/login',
                name: 'Login',
                component: Login,
                meta: {
                    needsAuth: false
                }
            },
            {
                path: '/registration',
                // name: 'Registration',
                component: Registration,
                name: 'Registration',
                meta: {
                    needsAuth: false
                }
            },
            {
                path: '/notes',
                component: Notes,
                name:'Notes',
                meta: {
                    needsAuth: true
                }
            }
        ]
    }
]

const router = new Router({
    routes,
    linkActiveClass: 'active',
    mode: 'history'
  })
  router.beforeEach(beforeEach)
  
  export default router