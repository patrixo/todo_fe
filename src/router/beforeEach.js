/* eslint-disable no-console */
import auth from '../auth'

const beforeEach = (to, from, next) => {
  if (to.meta.needsAuth === false) {
    return next()
  }
  auth.isLoggedIn().then(() => {
    next()
  }).catch(() => {
    auth.logout()
    return next({ name: 'Login' })
  })
}

export default beforeEach