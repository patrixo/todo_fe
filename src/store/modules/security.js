import axios from './../../axios'
import auth from './../../auth'

const webpage = {
  state: {
    user: null,
    loggedIn: false
  },
  mutations: {
    SET_USER (state, user) {
      state.user = user
      state.loggedIn = true
    },
    REMOVE_USER (state) {
      state.user = null,
      state.loggedIn = false
    }
  },
  getters: {
    isLoggedIn: (state) => {
      return state.loggedIn
    },
    loggedUser: (state) => {
      return state.user
    }
  },
  actions: {
    GET_USER ({ commit }) {
      return new Promise((resolve, reject) => {
        axios.get('/user').then(response => {
          delete response['token']
          commit('SET_USER', response)
          resolve(response)
        }).catch(error => {
          auth.logout(true)
          reject(error)
        })
      })
    },
    LOGIN ({ commit }, params) {
      return new Promise((resolve, reject) => {
        axios.post('/login', params).then(response => {
          const cmsAuthToken = response['token']
          const refreshToken = response['refreshToken']
          if (!cmsAuthToken) reject(new Error('Empty token'))
          delete response['token']
          Promise.all([auth.saveToken(cmsAuthToken), auth.saveRefreshToken({'refresh_token': refreshToken}), auth.saveUser(response)]).then(() => {
            resolve()
          })
        }).catch(error => {
          reject(error)
        })
      })
    },
    REGISTER ({ commit }, params) {
      return new Promise((resolve, reject) => {
        axios.post('/register', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // VERIFY_EMAIL ({ commit }, token) {
      VERIFY_EMAIL (token) {
      return new Promise((resolve, reject) => {
        axios.post('/verify-email', token).then(response => {
          const cmsAuthToken = response['api_token']
          if (!cmsAuthToken) reject(new Error('Empty token'))
          delete response['api_token']
          Promise.all([auth.saveToken(cmsAuthToken), auth.saveUser(response)]).then(() => {
            resolve()
          })
        }).catch(error => {
          reject(error)
        })
      })
    },
    async LOGOUT ({commit}) {
      const refreshToken = await auth.getRefreshToken()
      return new Promise((resolve, reject) => {
        axios.post('/logout', refreshToken).then(() => {
          commit('REMOVE_USER')
          auth.logout(true)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default webpage

//@TODO patrik logout zalozene na refresh tokene...
