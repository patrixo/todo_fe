import axios from './../../axios';

const webpage = {
  state: {
    notes: null
  },
  getters: {
    storedNotes: state => {
      return state.notes;
    }
  },
  actions: {
    GET_NOTES({commit}) {
			return new Promise((resolve, reject) => {
				axios.get('/notes').then(response => {
					commit('SET_NOTES', response)
					resolve(response)
				}).catch(error => {
          reject(error)
        })
			})
		},
    SAVE_NOTE({ commit }, note) {
			return new Promise((resolve, reject) => {
				axios.post('/note', note).then(response => {
					commit('SET_NOTES', response)
					resolve(response)
				}).catch(error => {
          reject(error)
        })
			})
		},
		DELETE_NOTE({ commit }, note) {
			return new Promise((resolve, reject) => {
				axios.delete(`/note/${note.id}`).then(response => {
					commit('SET_NOTES', response)
					resolve()
				}).catch(error => {
					reject(error)
				})
			})
		},
		UPDATE_NOTE({commit}, note) {
			return new Promise((resolve, reject) => {
				axios.patch('/note', note).then(response => {
					commit('SET_NOTES', response)
					resolve()
				}).catch(error => {
					reject(error)
				})
			})
		}
	},
	mutations: {
		SET_NOTES(state, notes) {
			state.notes = notes
		}
	}
};

export default webpage;
