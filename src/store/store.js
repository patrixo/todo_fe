import Vue from 'vue'
import Vuex from 'vuex'

import { security, notes } from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
      security,
      notes
    },
    state: {
      globalLoading: false,
      isMobile: false
    },
    mutations: {
      LOADING_SET (state, loading) {
        state.globalLoading = loading
      },
      IS_MOBILE_SET (state, bool) {
        state.isMobile = bool
      }
    }
  })