import axios from 'axios'
import auth from './auth'
import store from './store/store'

const instance  = axios.create({
    // baseUrl: process.env.VUE_APP_API_URL || 'http://127.0.0.1:3333'
    baseURL: 'http://127.0.0.1:3333'
})

instance.interceptors.response.use((response) => {
    store.commit('LOADING_SET', false)
    if (response.data) return response.data
  }, (error) => {
    store.commit('LOADING_SET', false)
    if (error.response && error.response.status && error.response.status === 401) {
      auth.logout()
    }
    // return Promise.reject(error.response.data)
  })
  
  instance.interceptors.request.use((config) => {
    store.commit('LOADING_SET', true)
    return config
  }, (error) => {
    return Promise.reject(error)
  })
  
  export default instance